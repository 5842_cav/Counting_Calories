﻿using System;
using NUnit.Framework;
using Window_Counting_Calories;

namespace UnitTestForWindowCountingCalories
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        [TestCase(110, 40, 30, 116)]
        public void PressCalculatorTest(int humanWeight, 
                                        int pressWeight, 
                                        int counts, 
                                        double expected)
        {
            var calculator = new PressCalculator(); 
            double calories = calculator.Calculate(humanWeight, pressWeight, counts);
            Assert.AreEqual(expected, calories);
        }

        [Test]
        [TestCase(90, 5, 10, 50)]
        public void RunCalculatorTest(int humanWeight, 
                                      int speed, 
                                      int distance, 
                                      double expected)
        {
            var calculator = new RunCalculator();
            double calories = calculator.Calculate(humanWeight, speed, distance);
            Assert.AreEqual(expected, calories);
        }

        [Test]
        [TestCase(80, 1, 1.1, 88)]
        public void SwimCalculatorTest(int humanWeight, 
                                       double time, 
                                       double multiplier, 
                                       double expected)
        {
            var calculator = new SwimCalculator(); 
            double calories = calculator.Calculate(humanWeight, time, multiplier);
            Assert.AreEqual(expected, calories);
        }

    }
}
