﻿using System;
using System.Threading;
using System.Globalization;
using System.Linq;
using Counting_Calories.Exercise;

namespace Counting_Calories_Console
{
	class Application
	{
		// Точка входа в консольное приложение
		static void Main(string[] args)
		{
			var program = new Application();
            Console.Title = "Подсчёт затраченных калорий";
           
            // Установка независимости от локального формата ввода чисел с запятой
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

			while (true)
			{
				Console.Clear();

				program.ShowMenu();

                Console.Write(" Введите номер упражнения: ");
				var Exercise = program.GetMenuItemNumber();

				switch (Exercise)
				{
					case 1:
						program.RunCounter();
						program.ExitExercise();
						break;
					case 2:
						program.SwimCounter();
						program.ExitExercise();
						break;
					case 3:
						program.PressCounter();
						program.ExitExercise();
						break;
					case 0:
						return;
					default:
						break;
				}
			}
		}

        /// <summary>
        /// Получить номер пункта меню в консоли
        /// </summary>
        /// <returns> Значение 1, 2, 3 либо 0 </returns>
        private int GetMenuItemNumber()
		{
			int[] menuItems = new int[] { 1, 2, 3, 0 };

			
			while (true)
			{
				var itemNumber = TryGetIntFromConsole();

				if (menuItems.Contains(itemNumber))
				{
					return itemNumber;
				}
				else
				{
					Console.Write("Неверно введённые данные. Попробуйте снова: ");
				}
			}
		}

        /// <summary>
        /// Выход из упражнения
        /// </summary>
        private void ExitExercise()
		{
			Console.WriteLine("Для продолжения нажмите любую клавишу...");
			Console.ReadKey();
		}
        
		private double TryGetDoubleFromConsole()
		{
			while (true)
			{
				try
				{
					var convertable = Convert.ToDouble(Console.ReadLine());
					return convertable;
				}
				catch
				{
					Console.Write("Неверно введённые данные. Введите значение типа Double: ");
				}
			}
		}
       
		private int TryGetIntFromConsole()
		{
			while (true)
			{
				try
				{
					int convertable = Convert.ToInt32(Console.ReadLine());
					return convertable;
				}
				catch
				{
					Console.Write("Неверно введённые данные. Введите значение типа Int: ");
				}
			}
		}

        /// <summary>
        /// Подпрограмма для вычисления килокалорий при беге
        /// </summary>
        private void RunCounter()
		{
			Console.Clear();            

			Console.Write("\n            Бег \n____________________________\n");
            Console.Write(" Введите заданные параметры:\n");

            Console.Write(" Вес (кг): ");
			var humanWeight = TryGetDoubleFromConsole();

			Console.Write(" Скорость бега (км/ч): ");
			var speed = TryGetDoubleFromConsole();
            while (speed == 0)
            {
                Console.Write(" Скорость бега должна быть больше 0. \n Попробуйте снова: ");
                speed = TryGetDoubleFromConsole();
            }

            Console.Write(" Расстояние (км): ");
			var distance = TryGetDoubleFromConsole();

			try
			{
				var run = new Run(humanWeight, speed, distance);
				Console.WriteLine("\n Сожжённые килокалории: {0}\n", run.CountCalories());
			}
			catch
			{
				Console.WriteLine(" Возникла ошибка при расчёте параметров. Пожалуйста, введите параметры заново.");
				RunCounter();
			}
		}

        /// <summary>
        /// Подпрограмма для вычисления килокалорий при плавании
        /// </summary>
        private void SwimCounter()
		{
			Console.Clear();

			Console.Write("\n           Плавание \n____________________________\n");
            Console.Write(" Введите заданные параметры:\n");

            Console.Write(" Вес (кг): ");
			var humanWeight = TryGetDoubleFromConsole();

			Console.Write(" Расстояние заплыва (км): ");
			var distance = TryGetDoubleFromConsole();

            Console.Write(" Скорость плаванья (км/ч) [средняя 5-6 км/ч]: ");
            var speed = TryGetDoubleFromConsole();
            while (speed == 0)
            {
                Console.Write(" Скорость должна быть больше 0. \n Попробуйте снова: ");
                speed = TryGetDoubleFromConsole();
            }

            Console.Write("     1 - Брасс\n     2 - Баттерфляй\n     3 - Кроль\n     4 - Удержание на воде \n Выберите стиль плавания: ");

            try
			{
                var swimmingStyle = GetSwimmingStyle();
                var swim = new Swim(humanWeight, swimmingStyle, distance, speed);
				Console.WriteLine("\n Сожжённые килокалории:  {0}\n", swim.CountCalories());
			}
			catch
			{
				Console.WriteLine("Возникла ошибка при расчёте параметров. Пожалуйста, введите параметры заново.");
				Console.ReadKey();
				SwimCounter();
			}
		}

        /// <summary>
        /// Получение названия стиля плавания по введенному номеру в консоли
        /// </summary>
        /// <returns> Выбранный стиль плаванья </returns>
        private SwimmingStyle GetSwimmingStyle()
		{
			while (true)
			{
				var styleNumber = TryGetIntFromConsole();

				switch (styleNumber)
				{
					case 1:
						return SwimmingStyle.Brass;
					case 2:
						return SwimmingStyle.Butterfly;
					case 3:
						return SwimmingStyle.Crawl;
                    case 4:
                        return SwimmingStyle.HoldingOnWater;
					default:
                        throw new ArgumentException("Неверный номер параметра. Пожалуйста, повторите ввод: ");
				}
			}
		}

        /// <summary>
        /// Подпрограмма для вычисления количества калорий при жиме штанги лёжа
        /// </summary>
        private void PressCounter()
		{
			Console.Clear();

			Console.Write("\n        Жим штанги лёжа \n____________________________\n");
            Console.Write(" Введите заданные параметры:\n");

            Console.Write(" Вес (кг): ");
			var humanWeight = TryGetDoubleFromConsole();

			Console.Write(" Поднимаемый вес (кг): ");
			var pressWeight = TryGetDoubleFromConsole();

			Console.Write(" Количество повторений: ");
			var repeats = TryGetIntFromConsole();

			try
			{
				var press = new BenchPress(humanWeight, pressWeight, repeats);
				Console.WriteLine("\n Сожжённые килокалории: {0}\n", press.CountCalories());
			}
			catch
			{
				Console.WriteLine(" Возникла ошибка при расчёте параметров. Пожалуйста, введите параметры заново.");
				PressCounter();
			}
		}

        /// <summary>
        /// Демонстрация консольного меню
        /// </summary>
		private void ShowMenu()
		{
			Console.WriteLine("\n Список упражнений: ");
            Console.WriteLine(" _______________________ ");
            Console.WriteLine(" 1 → Бег ");
			Console.WriteLine(" 2 → Плавание");
			Console.WriteLine(" 3 → Жим штанги лёжа");

            Console.WriteLine(" _______________________ ");
            Console.WriteLine(" 0 → Выйти из программы\n");
		}
	}
}
