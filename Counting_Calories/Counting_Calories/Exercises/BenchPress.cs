﻿using System;
using System.Collections.Generic;

namespace Counting_Calories.Exercise
{
    /// <summary>
    /// Класс "Жим лёжа на скамье"
    /// </summary>
    public class BenchPress : IExercise
    {
		// Вес поднимаемого груза
		private double _pressWeight;
        
        // Количество повторений
		private int _repeats;
        
		// Человеческий вес
		private double _humanWeight;

        /// <summary>
        /// Вес поднимаемого груза
        /// </summary>
        public double PressWeight
		{
			get { return _pressWeight; }
			set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _pressWeight = value;
            }
		}

        /// <summary>
        /// Количество повторений
        /// </summary>
		public int Repeats
		{
			get { return _repeats; }
			set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _repeats = value;
            }
		}

        /// <summary>
        /// Человеческий вес
        /// </summary>
        public double HumanWeight
        {
            get { return _humanWeight; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _humanWeight = value;
            }
        }

        /// <summary>
        /// Жим лёжа на скамье
        /// </summary>
        /// <param name="humanWeight"></param>
        /// <param name="pressWeight"></param>
        /// <param name="repeats"></param>
        public BenchPress(double humanWeight, double pressWeight, int repeats)
		{
			var parameters = new List<double>() { humanWeight, pressWeight, repeats };

			HumanWeight = humanWeight;
			PressWeight = pressWeight;
			Repeats = repeats;
		}

        /// <summary>
        /// Подсчет калорий при жиме лёжа
        /// </summary>
        /// <returns>Количество килокалорий</returns>
        public double CountCalories()
		{
			// Формула: (вес человека + поднимаемый вес) * количество повторов * 0.005
			return (_humanWeight + _pressWeight) *  _repeats * 0.005;
		}
		
	}
}
