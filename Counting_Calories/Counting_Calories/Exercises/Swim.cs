﻿using System;
using System.Collections.Generic;

namespace Counting_Calories.Exercise
{
    /// <summary>
    /// Стиль плавания
    /// </summary>
    public enum SwimmingStyle
    {
        Crawl,
        Butterfly,
        Brass,
        HoldingOnWater
    }

    /// <summary>
    /// Класс "Плавание"
    /// </summary>
    public class Swim : IExercise
    {
		// Стиль плавания
		private SwimmingStyle _style;
        
		// Расстояние плавания в километрах
		private double _distance;
        
		// Человеческий вес
		private double _humanWeight;

        // Скорость плаванья
        private double _speed;

        /// <summary>
        /// Стиль плавания
        /// </summary>
        public SwimmingStyle Style
		{
			get { return _style; }
			set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _style = value;
            }
		}

        /// <summary>
        /// Расстояние плавания в километрах
        /// </summary>
        public double Distance
		{
			get { return _distance; }
			set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _distance = value;
            }
		}

        /// <summary>
        /// Человеческий вес
        /// </summary>
        public double HumanWeight
        {
            get { return _humanWeight; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _humanWeight = value;
            }
        }

        /// <summary>
        /// Скорость плаванья
        /// </summary>
        public double Speed
        {
            get { return _speed; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _speed = value;
            }
        }

        /// <summary>
        /// Конструктор объекта "Плавание" с параметрами
        /// </summary>
        /// <param name="humanWeight">Вес человека</param>
        /// <param name="style"> Стиль плаванья</param>
        /// <param name="distance">Расстояние</param>
        /// <param name="speed">Скорость плаванья</param>
        public Swim(double humanWeight, SwimmingStyle style, double distance, double speed)
		{
			var parameters = new List<double>() { humanWeight, distance };

			HumanWeight = humanWeight;
			Distance = distance;
			_style = style;
            Speed = speed;

        }

        /// <summary>
        /// Подсчет калории при плавании
        /// </summary>
        /// <returns>Количество килокалорий при плавании в зависимости от вида упражнений</returns>
        public double CountCalories()
		{
            // Время плавания = расстояние / скорость плавания (в среднем 5-6 км/ч)
            var time = _distance / _speed;
            // Брас: вес * время * 10
            // Кроль и баттерфляй: вес * время * 11
            // Удержание на воде: вес * время * 4
            var answer = 0.0;
			switch (_style)
			{
				case SwimmingStyle.Brass:

					answer = _humanWeight * time * 10;
					break;
				case SwimmingStyle.Butterfly:
				case SwimmingStyle.Crawl:
					answer = _humanWeight * time * 11;
					break;
                case SwimmingStyle.HoldingOnWater:
                    answer = _humanWeight * time * 4;
                    break;
                default:
                    throw new ArgumentException("Не существующий стиль плавания");
			}
			return answer;
		}
		
	}
}
