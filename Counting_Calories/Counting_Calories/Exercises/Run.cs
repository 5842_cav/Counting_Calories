﻿using System;
using System.Collections.Generic;

namespace Counting_Calories.Exercise
{
	// Класс "Бег"
	public class Run : IExercise
    {
		// Интенсивность(скорость) бега
		private double _speed;
        
		// Расстояние бега в километрах
		private double _distance;
        
		// Человеческий вес
		private double _humanWeight;

        /// <summary>
        /// Скорость бега
        /// </summary>
        public double Speed
		{
			get { return _speed; }
			set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _speed = value;
            }
		}

        /// <summary>
        /// Расстояние бега в километрах
        /// </summary>
        public double Distance
		{
			get { return _distance; }
			set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _distance = value;
            }
		}

        /// <summary>
        /// Человеческий вес
        /// </summary>
        public double HumanWeight
        {
            get { return _speed; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                _humanWeight = value;
            }
        }

        /// <summary>
        /// Конструктор объекта "Бег" с параметрами
        /// </summary>
        /// <param name="humanWeight"></param>
        /// <param name="speed"></param>
        /// <param name="distance"></param>
        public Run(double humanWeight, double speed, double distance)
		{
			var parameters = new List<double>() { humanWeight, speed, distance };

			HumanWeight = humanWeight;
			_distance = distance;
            Speed = speed;
		}

        /// <summary>
        /// Подсчет калорий при беге
        /// </summary>
        /// <returns>Количество килокалорий при беге</returns>
        public double CountCalories()
		{
			// Время бега = расстояние бега / скорость бега
			var time = _distance / _speed;
			// Формула: скорость * вес человека * время
			return _humanWeight * _speed * time;
		}
		
	}
}
