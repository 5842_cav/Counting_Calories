﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counting_Calories.Exercise
{
    /// <summary>
    /// Интерфейс для упражнений
    /// </summary>
    interface IExercise
    {
		// Посчитать количество калорий, затраченных при выполнении упражнения
		double CountCalories();
	}
}
