﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Window_Counting_Calories
{
    public class PressCalculator
    {
        public double Calculate(int humanWeight, 
                                int pressWeight, 
                                int counts)
        {
            return humanWeight + pressWeight * counts * 0.005;
        }
    }

}
