﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Window_Counting_Calories
{
    public class SwimCalculator
    {
        public double Calculate(int humanWeight, 
                                double time, 
                                double multiplier)
        {
            return humanWeight * time * multiplier;
        }
    }

}
