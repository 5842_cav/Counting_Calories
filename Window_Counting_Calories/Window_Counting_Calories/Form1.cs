﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Window_Counting_Calories
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static class StaticVariables{

            public static DataSet ds;
        }

        public OleDbDataAdapter adapter = new OleDbDataAdapter();
        public DataSet ds = new DataSet();

        /// <summary>
        /// Выполнение позле загрузки формы
        /// </summary>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                ///Подключение БД к DataGridView1  
                OleDbConnection connection = new OleDbConnection();
                connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Project_NTvP\Counting_Calories\Window_Counting_Calories\CountingCaloriesE.accdb";
                DataTable dt = new DataTable();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "SELECT * from Counting_Calories";
                OleDbCommandBuilder bulder = new OleDbCommandBuilder(adapter);
                connection.Open();
                adapter.SelectCommand = command;
                adapter.Fill(ds);
                adapter.UpdateCommand = bulder.GetUpdateCommand();
                adapter.InsertCommand = bulder.GetInsertCommand();
                adapter.DeleteCommand = bulder.GetDeleteCommand();
                dt = ds.Tables[0];
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["ID"].Visible = false;
                dataGridView1.Columns["ArrOfPress"].Visible = false;
                dataGridView1.Columns["ArrOfRun"].Visible = false;
                dataGridView1.Columns["ArrOfSwim"].Visible = false;
                
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }

        }

        /// <summary>
        /// Открытие формы ADD
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Form ADDForm = new UpdateForm(ds, adapter);
            ADDForm.ShowDialog();
        }

        /// <summary>
        /// Открытие формы EDIT
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            Form EDITForm = new UpdateForm(ds, adapter, ds.Tables[0].Rows[dataGridView1.CurrentRow.Index], dataGridView1.CurrentRow.Index);
            EDITForm.ShowDialog();
        }

        /// <summary>
        /// Удаление выделеной строки
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            ds.Tables[0].Rows[dataGridView1.CurrentRow.Index].Delete();
            adapter.Update(ds.Tables[0]);
        }
        
        /// <summary>
        /// Открытие БД
        /// </summary>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Выберите файл";
                openFileDialog1.Filter = "База данных Access|*.accdb";
                openFileDialog1.ShowDialog();
                adapter.SelectCommand.Connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openFileDialog1.FileName;
                ds.Tables[0].Clear();
                adapter.Fill(ds.Tables[0]);


                dataGridView1.DataSource = ds.Tables[0];
                adapter.Update(ds.Tables[0]);

                dataGridView1.Columns["ID"].Visible = false;
                dataGridView1.Columns["ArrOfPress"].Visible = false;
                dataGridView1.Columns["ArrOfRun"].Visible = false;
                dataGridView1.Columns["ArrOfSwim"].Visible = false;
                

            } catch (Exception ex)
            {
                MessageBox.Show("Error: \n Выбранная база данных не подходить для программы." +
                                "Попробуйте другую! \n" + ex.Message);
            }
        }
        /// <summary>
        /// Сохранить текущую таблицу в БД
        /// </summary>
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                adapter.Update(ds.Tables[0]);
                MessageBox.Show("База данных была сохранена!");
            } catch (Exception ex)
            {
                MessageBox.Show("Error: \n База данных не была сохранена \n" + ex.Message);
            }
        }

        /// <summary>
        /// Информация о приложении и авторе
        /// </summary>
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Приложение Counting Calories\n\n" +
                "Приложение расчёта сожжёных калорий\n" +
                "для упражнений: Бег, Жим штанги лёжа, Плавание.\n " +
                "\nСоздано Анастасией Чередниченко", "Информация о приложении");
        }

        /// <summary>
        /// Заполнение полей для быстрых расчётов
        /// </summary>
        private void dataGridView1_CellClick(object sender, 
                                             DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentCell.ColumnIndex == 2)
            {
                this.Width = 600;
                string[] arr;
                DataRow row = ds.Tables[0].Rows[dataGridView1.CurrentRow.Index];
                switch (row[1].ToString())
                {
                    case "Жим лёжа":
                        arr = row[3].ToString().Split(',');
                        tabControl1.SelectedIndex = 1;
                        textBox9.Text = arr[0];
                        textBox7.Text = arr[1];
                        textBox8.Text = arr[2];
                        break;
                    case "Плавание":
                        arr = row[5].ToString().Split(',');
                        tabControl1.SelectedIndex = 2;
                        textBox6.Text = arr[0];
                        textBox5.Text = arr[1];
                        textBox4.Text = arr[2];
                        switch (arr[3])
                        {
                            case "Брас":
                                comboBox2.SelectedIndex = 0;
                                break;
                            case "Баттерфляй":
                                comboBox2.SelectedIndex = 1;
                                break;
                            case "Кроль":
                                comboBox2.SelectedIndex = 2;
                                break;
                            case "Удержание на воде":
                                comboBox2.SelectedIndex = 3;
                                break;
                        }
                        break;
                    case "Бег":
                        arr = row[4].ToString().Split(',');
                        tabControl1.SelectedIndex = 0;
                        textBox1.Text = arr[0];
                        textBox2.Text = arr[1];
                        textBox3.Text = arr[2];
                        break;
                }
            } else
            {
                this.Width = 295;
            }
        }

        /// <summary>
        /// Быстрый расчёт и занесение значений в таблицу
        /// </summary>
        private void TextChanged_Method(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell.ColumnIndex == 2) { 
                int index = dataGridView1.CurrentRow.Index;
                // Выбор действия по открытой вкладке tabControl1
                switch (tabControl1.SelectedIndex)
                {
                    // Вкладка "Жим лёжа"
                    case 1:
                        try
                        {
                            var calculator = new PressCalculator();
                            var humanWeight = Convert.ToInt32(textBox9.Text);
                            var pressWeight = Convert.ToInt32(textBox7.Text);
                            var counts = Convert.ToInt32(textBox8.Text);
                            double calories = calculator.Calculate(humanWeight,
                                                                   pressWeight,
                                                                   counts);
                            if (humanWeight > 0 & pressWeight > 0 & counts > 0)
                            {
                                DataRow row = ds.Tables[0].Rows[index];
                                row.SetField(0, row[0]);
                                row.SetField(1, "Жим лёжа");
                                row.SetField(2, calories);
                                row.SetField(4, "");
                                row.SetField(5, "");
                                row.SetField(3, (humanWeight + "," +
                                                 pressWeight + "," +
                                                 counts).ToString());
                                adapter.Update(ds.Tables[0]);

                                this.BackColor = Color.White;
                            }
                        }
                        catch
                        {
                            this.BackColor = Color.Red;
                        }
                        break;
                    // Вкладка "Плавание"
                    case 2:
                        try
                        {
                            var calculator = new SwimCalculator();
                            var distance = Convert.ToInt32(textBox5.Text);
                            var speed = Convert.ToInt32(textBox4.Text);
                            var humanWeight = Convert.ToInt32(textBox6.Text);
                            if (distance > 0 & speed > 0 & humanWeight > 0)
                            {
                                double time = distance / speed;
                                double calories = 0;
                                // Расчёт от выбранного стиля плавания
                                switch (comboBox2.SelectedIndex)
                                {
                                    case 0:
                                        calories = calculator.Calculate(humanWeight,
                                                                        time,
                                                                        1);
                                        break;
                                    case 1:
                                    case 2:
                                        calories = calculator.Calculate(humanWeight,
                                                                        time,
                                                                        1.1);
                                        break;
                                    case 3:
                                        calories = calculator.Calculate(humanWeight,
                                                                        time,
                                                                        0.4);
                                        break;
                                    default:
                                        break;
                                }

                                DataRow row = ds.Tables[0].Rows[index];
                                row.SetField(0, row[0]);
                                row.SetField(1, "Плавание");
                                row.SetField(2, calories);
                                row.SetField(3, "");
                                row.SetField(4, "");
                                row.SetField(5, (humanWeight + "," +
                                                 distance + "," +
                                                 speed + "," +
                                                 comboBox2.Text).ToString());
                                adapter.Update(ds.Tables[0]);

                                this.BackColor = Color.White;
                            }
                        }
                        catch
                        {
                            this.BackColor = Color.Red;
                        }
                        break;
                    // Оставшаяся вкладка "Бег"
                    default:
                        try
                        {

                            var calculator = new RunCalculator();
                            var humanWeight = Convert.ToInt32(textBox1.Text);
                            var speed = Convert.ToInt32(textBox2.Text);
                            var distance = Convert.ToInt32(textBox3.Text);
                            double calories = calculator.Calculate(humanWeight,
                                                                   speed,
                                                                   distance);
                            if (distance > 0 & speed > 0 & humanWeight > 0)
                            {
                                DataRow row = ds.Tables[0].Rows[index];
                                row.SetField(0, row[0]);
                                row.SetField(1, "Бег");
                                row.SetField(2, calories);
                                row.SetField(3, "");
                                row.SetField(5, "");
                                row.SetField(4, (humanWeight + "," +
                                                 speed + "," +
                                                 distance).ToString());
                                adapter.Update(ds.Tables[0]);

                                this.BackColor = Color.White;
                            }
                        }
                        catch
                        {
                            this.BackColor = Color.Red;
                        }
                        break;
                }
            }
        }
    }
}
