﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Window_Counting_Calories
{
    public partial class UpdateForm : Form
    {
        public DataSet ds;
        public OleDbDataAdapter adapter;
        //Переменная определяет действие: "Добавить" или "Изменить"
        //если добавляем строку, то = "-1", если изменяем строку, то = Int > 0
        public int index = -1;

        /// <summary>
        /// Конструктор без входных значений
        /// </summary>
        public UpdateForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Конструктор с 2мя входными значениями
        /// </summary>
        /// <param name="DS">область хранения инф-и из бд</param>
        /// <param name="Adapt">функционал работы с бд</param>
        public UpdateForm(DataSet DS, OleDbDataAdapter Adapt)
        {
            InitializeComponent();
            ds = DS;
            adapter = Adapt;
        }

        /// <summary>
        /// Конструктор с 4мя входными значениями
        /// </summary>
        /// <param name="DS">область хранения инф-и из бд</param>
        /// <param name="Adapt">функционал работы с бд</param>
        /// <param name="row">активная строка бд</param>
        /// <param name="Indx">индекс активной строки</param>
        public UpdateForm(DataSet DS, 
                          OleDbDataAdapter Adapt, 
                          DataRow row, 
                          int Indx)
        {
            InitializeComponent();
            ds = DS;
            adapter = Adapt;
            index = Indx;
            string[] arr;
            switch (row[1].ToString())
            {
                case "Жим лёжа":
                    arr = row[3].ToString().Split(',');
                    tabControl1.SelectedIndex = 1;
                    textBox9.Text = arr[0];
                    textBox7.Text = arr[1];
                    textBox8.Text = arr[2];
                    break;
                case "Плавание":
                    arr = row[5].ToString().Split(',');
                    tabControl1.SelectedIndex = 2;
                    textBox6.Text = arr[0];
                    textBox5.Text = arr[1];
                    textBox4.Text = arr[2];
                    switch (arr[3])
                    {
                        case "Брас":
                            comboBox2.SelectedIndex = 0;
                            break;
                        case "Баттерфляй":
                            comboBox2.SelectedIndex = 1;
                            break;
                        case "Кроль":
                            comboBox2.SelectedIndex = 2;
                            break;
                        case "Удержание на воде":
                            comboBox2.SelectedIndex = 3;
                            break;
                    }
                    break;
                case "Бег":
                    arr = row[4].ToString().Split(',');
                    tabControl1.SelectedIndex = 0;
                    textBox1.Text = arr[0];
                    textBox2.Text = arr[1];
                    textBox3.Text = arr[2];
                    break;
            }
        }



        /// <summary>
        /// Выполнение добавления/изменения строки в таблице или вывод ошибки
        /// </summary>
        private void button1_Click(object sender, 
                                   EventArgs e)
        {
            Boolean closeFlag = true;
            if (index < 0)
            {
                // Выбор действия по открытой вкладке tabControl1
                switch (tabControl1.SelectedIndex)
                {
                    // Вкладка "Жим лёжа"
                    case 1:
                        try
                        {
                            var calculator = new PressCalculator();
                            var humanWeight = Convert.ToInt32(textBox9.Text);
                            var pressWeight = Convert.ToInt32(textBox7.Text);
                            var counts = Convert.ToInt32(textBox8.Text);
                            if (pressWeight > 0 & counts > 0 & humanWeight > 0)
                            {
                                double calories = calculator.Calculate(humanWeight, 
                                                                   pressWeight, 
                                                                   counts);

                            ds.Tables[0].Rows.Add(ds.Tables[0].Rows.Count + 1, 
                                                  "Жим лёжа", calories, 
                                                  (humanWeight + "," + 
                                                  pressWeight + "," + 
                                                  counts).ToString(), 
                                                  null, 
                                                  null);
                            adapter.Update(ds.Tables[0]);

                            closeFlag = true;
                        }
                            else
                            {
                                closeFlag = false;
                                MessageBox.Show("Данные заданы не корректно.");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Попробуйте ввести данные ещё раз: \n" + ex, 
                                            "Ошибка");
                            closeFlag = false;
                        }
                        break;
                    // Вкладка "Плавание"
                    case 2:
                        try
                        {
                            var calculator = new SwimCalculator();
                            var distance = Convert.ToInt32(textBox5.Text);
                            var speed = Convert.ToInt32(textBox4.Text);
                            var humanWeight = Convert.ToInt32(textBox6.Text);
                            if (speed > 0 & distance > 0 & humanWeight > 0)
                            {
                            double time = distance / speed;
                            double calories = 0;
                            closeFlag = true;
                            // Расчёт от выбранного стиля плавания
                            switch (comboBox2.SelectedIndex)
                            {
                                case 0:
                                    calories = calculator.Calculate(humanWeight, 
                                                                    time, 
                                                                    1);
                                    break;
                                case 1:
                                case 2:
                                    calories = calculator.Calculate(humanWeight, 
                                                                    time, 
                                                                    1.1);
                                    break;
                                case 3:
                                    calories = calculator.Calculate(humanWeight, 
                                                                    time, 
                                                                    0.4);
                                    break;
                                default:
                                    MessageBox.Show("Выберете стиль плаванья", 
                                                    "Ошибка");
                                    closeFlag = false;
                                    break;
                            }

                            ds.Tables[0].Rows.Add(ds.Tables[0].Rows.Count + 1, 
                                                  "Плавание", calories, null, null, 
                                                  (humanWeight + "," + distance + "," + 
                                                  speed + "," + comboBox2.Text).ToString());
                            adapter.Update(ds.Tables[0]);
                            }
                            else
                            {
                                closeFlag = false;
                                MessageBox.Show("Данные заданы не корректно.");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Попробуйте ввести данные ещё раз: \n" + ex.Message, 
                                            "Ошибка");
                            closeFlag = false;
                        }
                        break;
                    // Оставшаяся вкладка "Бег" 
                    default:
                        try
                        {
                            var calculator = new RunCalculator();
                            var humanWeight = Convert.ToInt32(textBox1.Text);
                            var speed = Convert.ToInt32(textBox2.Text);
                            var distance = Convert.ToInt32(textBox3.Text);
                            if (speed > 0 & distance > 0 & humanWeight > 0)
                            {
                                double calories = calculator.Calculate(humanWeight,
                                                                   speed,
                                                                   distance);
                                ds.Tables[0].Rows.Add(ds.Tables[0].Rows.Count + 1,
                                                      "Бег", calories, null,
                                                      (humanWeight + "," + speed + "," +
                                                      distance).ToString(),
                                                      null);
                                adapter.Update(ds.Tables[0]);

                                closeFlag = true;
                            }
                            else
                            {
                                closeFlag = false;
                                MessageBox.Show("Данные заданы не корректно.");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Попробуйте ввести данные ещё раз: \n" + ex.Message,
                                            "Ошибка");
                            closeFlag = false;
                        }
                        break;
                }
                if (closeFlag) this.Close();
            } else
            {
                // Выбор действия по открытой вкладке tabControl1
                switch (tabControl1.SelectedIndex)
                {
                    // Вкладка "Жим лёжа"
                    case 1:
                        try
                        {
                            var calculator = new PressCalculator();
                            var humanWeight = Convert.ToInt32(textBox9.Text);
                            var pressWeight = Convert.ToInt32(textBox7.Text);
                            var counts = Convert.ToInt32(textBox8.Text);
                            if (pressWeight > 0 & counts > 0 & humanWeight > 0)
                            {
                                double calories = calculator.Calculate(humanWeight,
                                                                   pressWeight,
                                                                   counts);

                                DataRow row = ds.Tables[0].Rows[index];
                                row.SetField(0, row[0]);
                                row.SetField(1, "Жим лёжа");
                                row.SetField(2, calories);
                                row.SetField(4, "");
                                row.SetField(5, "");
                                row.SetField(3, (humanWeight + "," + pressWeight + "," +
                                                 counts).ToString());
                                adapter.Update(ds.Tables[0]);

                                closeFlag = true;
                            }
                            else
                            {
                                closeFlag = false;
                                MessageBox.Show("Данные заданы не корректно.");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Попробуйте ввести данные ещё раз: \n" + ex.Message,
                                            "Ошибка");
                            closeFlag = false;
                        }
                        break;
                    // Вкладка "Плавание"
                    case 2:
                        try
                        {
                            var calculator = new SwimCalculator();
                            var distance = Convert.ToInt32(textBox5.Text);
                            var speed = Convert.ToInt32(textBox4.Text);
                            var humanWeight = Convert.ToInt32(textBox6.Text);
                            if (distance > 0 & speed > 0 & humanWeight > 0)
                            {
                                double time = distance / speed;
                                double calories = 0;
                                closeFlag = true;
                                // Расчёт от выбранного стиля плавания
                                switch (comboBox2.SelectedIndex)
                                {
                                    case 0:
                                        calories = calculator.Calculate(humanWeight, time, 1);
                                        break;
                                    case 1:
                                    case 2:
                                        calories = calculator.Calculate(humanWeight, time, 1.1);
                                        break;
                                    case 3:
                                        calories = calculator.Calculate(humanWeight, time, 0.4);
                                        break;
                                    default:
                                        MessageBox.Show("Выберете стиль плаванья", "Ошибка");
                                        closeFlag = false;
                                        break;
                                }

                                DataRow row = ds.Tables[0].Rows[index];
                                row.SetField(0, row[0]);
                                row.SetField(1, "Плавание");
                                row.SetField(2, calories);
                                row.SetField(3, "");
                                row.SetField(4, "");
                                row.SetField(5, (humanWeight + "," + distance + "," +
                                                 speed + "," + comboBox2.Text).ToString());
                                adapter.Update(ds.Tables[0]);

                            }
                            else
                                {
                                    closeFlag = false;
                                    MessageBox.Show("Данные заданы не корректно.");
                                }
                            }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Попробуйте ввести данные ещё раз: \n" + ex.Message,
                                            "Ошибка");
                            closeFlag = false;
                        }
                        break;
                    // Оставшаяся вкладка "Бег"
                    default:
                        try
                        {

                            var calculator = new RunCalculator();
                            var humanWeight = Convert.ToInt32(textBox1.Text);
                            var speed = Convert.ToInt32(textBox2.Text);
                            var distance = Convert.ToInt32(textBox3.Text);
                            double calories = calculator.Calculate(humanWeight,
                                                                   speed,
                                                                   distance);
                            if (distance > 0 & speed > 0 & humanWeight > 0)
                            {
                                DataRow row = ds.Tables[0].Rows[index];
                                row.SetField(0, row[0]);
                                row.SetField(1, "Бег");
                                row.SetField(2, calories);
                                row.SetField(3, "");
                                row.SetField(5, "");
                                row.SetField(4, (humanWeight + "," + speed + "," +
                                                 distance).ToString());
                                adapter.Update(ds.Tables[0]);

                                closeFlag = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Попробуйте ввести данные ещё раз: \n" + ex.Message,
                                            "Ошибка");
                            closeFlag = false;
                        }
                        break;
                }
                if (closeFlag) this.Close();
            }
        }
    }
}
