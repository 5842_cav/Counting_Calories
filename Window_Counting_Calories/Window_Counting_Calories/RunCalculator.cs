﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Window_Counting_Calories
{
    public class RunCalculator
    {
        public double Calculate(int humanWeight, 
                                int speed, 
                                int distance)
        {
            if (humanWeight == 0 ) return 0;
            return humanWeight * speed * distance / humanWeight;
        }
    }

}
